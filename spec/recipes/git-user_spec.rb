require_relative "../spec_helper"

describe "gitlab-omnibus-prerequisites::git-user" do
  context 'When the user is created for the first time with default options.' do
    let(:chef_run) {
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node, _server|
        node.normal['etc']['group']['git'] = nil
        node.normal['etc']['passwd']['git'] = nil
        node.normal['gitlab-omnibus-prerequisites']['user']['uid'] = 500
        node.normal['gitlab-omnibus-prerequisites']['user']['gid'] = 500
      end.converge(described_recipe)
    }

    it "creates the group" do
      expect(chef_run).to create_group('git').with(gid: 500, system:true)
    end

    it "creates the user" do
      expect(chef_run).to create_user('git')
        .with(shell: '/bin/sh',home: '/var/opt/gitlab', uid: 500, gid: 500, system: true, password: nil)
    end
  end

  context 'When the user is already on the system' do
    let(:chef_run) {
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node, _server|
        node.normal['etc']['passwd']['git'] = true
        node.normal['etc']['group']['git'] = true
      end.converge(described_recipe)
    }

    it "does not create the user" do
      expect(chef_run).to_not create_user('git')
    end
  end

  context 'When the user is already on the system with a forced password update' do
    let(:chef_run) {
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node, _server|
        node.normal['etc']['passwd']['git'] = true
        node.normal['etc']['group']['git'] = true
        node.normal['gitlab-omnibus-prerequisites']['user']['allow_updates'] = true
        node.normal['gitlab-omnibus-prerequisites']['user']['shadow_value'] = '*'
        node.normal['gitlab-omnibus-prerequisites']['user']['uid'] = 500
        node.normal['gitlab-omnibus-prerequisites']['user']['gid'] = 500
      end.converge(described_recipe)
    }

    it "creates the user" do
      expect(chef_run).to create_user('git')
        .with(shell: '/bin/sh',home: '/var/opt/gitlab', uid: 500, gid: 500, system: true, password: '*')
    end
  end

end
