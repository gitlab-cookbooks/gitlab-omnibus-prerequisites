user_settings = node['gitlab-omnibus-prerequisites']['user']

group user_settings['group'] do
	gid user_settings['gid']
	system true
	not_if { node['etc']['group'][user_settings['group']] }
end

shadow_value = node['gitlab-omnibus-prerequisites']['user']['shadow_value']
disallow_updates = !node['gitlab-omnibus-prerequisites']['user']['allow_updates']
user_exists = !node['etc']['passwd'][user_settings['username']].nil?

user user_settings['username'] do
  shell user_settings['shell']
  home user_settings['home']
  uid user_settings['uid']
  gid user_settings['gid']
  system true
  # When usepam is set to false the shadow entry must
  # be '*', not '!'. We set this for the git user
  password shadow_value unless shadow_value.nil?

  ## *Always* updates the user if allow_updates is set to true.
  ## This should be done with care as if the UID changes it can result
  ## in permission issues.  Otherwise, this is skipped if the user
  ## is already on the system.
  not_if { disallow_updates && user_exists }
end

