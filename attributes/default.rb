default['gitlab-omnibus-prerequisites']['user']['username'] = "git"
default['gitlab-omnibus-prerequisites']['user']['group'] = "git"
default['gitlab-omnibus-prerequisites']['user']['uid'] = 1100
default['gitlab-omnibus-prerequisites']['user']['gid'] = 1100
default['gitlab-omnibus-prerequisites']['user']['shell'] = "/bin/sh"
default['gitlab-omnibus-prerequisites']['user']['home'] = "/var/opt/gitlab"
default['gitlab-omnibus-prerequisites']['user']['allow_updates'] = false
default['gitlab-omnibus-prerequisites']['user']['shadow_value'] = nil
