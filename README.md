# gitlab-omnibus-prerequisites
This cookbook should prepare the servers for the omnibus installation. In most cases this is not needed, however since we are running distributed git infrastructure with nfs, we have to prepare the server BEFORE the omnibus package is installed so we dont fail because of missing shares, folders etc.

## Attributes

no attributes

## Usage

### gitlab-omnibus-prerequisites::git-user

Include this in your run list to create the git user we need.

