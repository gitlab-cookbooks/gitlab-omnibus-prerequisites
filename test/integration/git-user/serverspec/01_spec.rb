require "spec_helper"


describe "Git group exists" do
  context group("git") do
    it { should exist }
    it { should have_gid 1100 }
  end
end

describe "Git user exists" do
  context user("git") do
    it { should exist }
    it { should have_uid 1100 }
    it { should belong_to_group 'git' }
    it { should have_home_directory '/var/opt/gitlab' }
    it { should have_login_shell '/bin/sh' }
  end
end
